import type { Context } from '@/deps.ts';

import { url } from '@/utils/parse.ts';

function nodeInfoController(c: Context) {
  return c.json({
    links: [
      {
        rel: 'http://nodeinfo.diaspora.software/ns/schema/2.0',
        href: url('/nodeinfo/2.0'),
      },
      {
        rel: 'http://nodeinfo.diaspora.software/ns/schema/2.1',
        href: url('/nodeinfo/2.1'),
      },
    ],
  });
}

function nodeInfoSchemaController(c: Context<'version'>) {
  return c.json({
    version: '2.1',
    software: {
      name: 'mostr',
      version: '1.0.0',
      repository: 'https://gitlab.com/soapbox-pub/mostr',
      homepage: 'https://mostr.pub',
    },
    protocols: [
      'activitypub',
    ],
    services: {
      inbound: [],
      outbound: [],
    },
    openRegistrations: false,
    usage: {
      users: {
        total: 0,
        activeMonth: 0,
        activeHalfyear: 0,
      },
      localPosts: 0,
      localComments: 0,
    },
    metadata: {
      features: [
        'nip05',
        'nostr_bridge',
      ],
    },
  });
}

export { nodeInfoController, nodeInfoSchemaController };
