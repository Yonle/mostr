import { nip19 } from '@/deps.ts';

import type { MiddlewareHandler } from '@/deps.ts';

export const nip19Redirect = (): MiddlewareHandler<'pubkey' | 'id'> => {
  return async (c, next) => {
    const nostrId = c.req.param('pubkey') || c.req.param('id');

    try {
      const decoded = nip19.decode(nostrId);
      console.log(`Redirecting ${c.req.url}`);

      switch (decoded.type) {
        case 'npub':
          return c.redirect(`/users/${decoded.data}`);
        case 'nprofile':
          // TODO: add `relays` as a query param to actor endpoint?
          return c.redirect(`/users/${decoded.data.pubkey}`);
        case 'note':
          return c.redirect(`/objects/${decoded.data}`);
        case 'nevent':
          return c.redirect(`/objects/${decoded.data.id}`);
      }
    } catch {
      // do nothing.
    }

    await next();
  };
};
