import { maybePublishActor } from './publisher.ts';

import { NOSTR_RELAY } from '../config.ts';
import { cipher } from '../db.ts';
import { getPublicKey } from './sign.ts';
import { finger } from '../webfinger/finger.ts';

import type { Context } from '@/deps.ts';

/**
 * NIP-05 implementation.
 * <https://github.com/nostr-protocol/nips/blob/master/05.md>
 */
async function nostrController(c: Context) {
  const name = c.req.query('name');

  if (!/^[a-z0-9.-]+_at_[a-z0-9.-]+$/i.test(name)) {
    return c.json({ error: 'Invalid name.' }, 400);
  }

  const acct = name.replace('_at_', '@');
  const actor = await finger(acct);

  if (actor) {
    maybePublishActor(actor);
    const pubkey = await getPublicKey(actor.id);

    new Promise<void>((resolve) => {
      cipher.add({ apId: actor.id, nostrId: pubkey });
      resolve();
    });

    return c.json({
      names: {
        [name]: pubkey,
      },
      relays: {
        [pubkey]: [NOSTR_RELAY],
      },
    });
  } else {
    return c.json({ error: 'Not found' }, 404);
  }
}

export { nostrController };
