import { buildFollow, wrapCreate, wrapUpdate } from '../activitypub/builder.ts';
import { federate } from '../activitypub/federation.ts';
import { toActor, toAnnounce, toDelete, toEmojiReact, toLike, toNote, toZap } from '../activitypub/transmute.ts';
import { cipher, followsDB } from '../db.ts';
import { url } from '../utils/parse.ts';

import { getFollowsDiff } from './follows.ts';
import { isProxyEvent, isQuoteTag } from './tags.ts';

import type { Event } from './event.ts';
import { LOCAL_DOMAIN } from '../config.ts';

function handleEvent(event: Event): void {
  if (!isEventEligible(event)) return;

  switch (event.kind) {
    case 0:
      handleEvent0(event as Event<0>);
      return;
    case 1:
      handleEvent1(event as Event<1>);
      return;
    case 3:
      handleEvent3(event as Event<3>);
      return;
    case 5:
      handleEvent5(event as Event<5>);
      return;
    case 6:
      handleEvent6(event as Event<6>);
      return;
    case 7:
      handleEvent7(event as Event<7>);
      return;
    case 9735:
      handleEvent9735(event as Event<9735>);
      return;
  }
}

async function handleEvent0(event: Event<0>) {
  const cache = await caches.open('web');
  await cache.delete(new URL(`/users/${event.pubkey}`, LOCAL_DOMAIN));
  return federate(wrapUpdate(await toActor(event)));
}

async function handleEvent1(event: Event<1>) {
  if (isRepost(event)) {
    const activity = toAnnounce(event);
    if (activity) {
      return federate(activity);
    }
  } else {
    return federate(wrapCreate(await toNote(event)));
  }
}

function handleEvent3(event: Event<3>) {
  const actorId = cipher.getApId(event.pubkey);
  if (actorId) return; // Prevent loops

  const diff = getFollowsDiff(event);

  diff.follow.forEach((apId) => {
    const follow = buildFollow(url(`/users/${event.pubkey}`), apId);
    federate(follow);
    followsDB.addFollow(event.pubkey, apId);
  });

  // TODO: Unfollow
}

function handleEvent5(event: Event<5>) {
  const activity = toDelete(event);

  if (activity) {
    return federate(activity);
  }
}

// Note: Event<6> is deprecated, but because it's still in use we handle it.
// Clients should prefer Event<1> with a mention, and the bridge should never
// send Event<6>.
function handleEvent6(event: Event<6>) {
  const tag = event.tags.find(isQuoteTag);

  if (tag) {
    const activity = toAnnounce(event);
    if (activity) {
      return federate(activity);
    }
  }
}

function handleEvent7(event: Event<7>) {
  if (event.content === '+' || event.content === '') {
    const like = toLike(event);
    if (like) {
      return federate(like);
    }
  } else if (/\p{Extended_Pictographic}/u.test(event.content)) {
    const emojiReact = toEmojiReact(event);
    if (emojiReact) {
      return federate(emojiReact);
    }
  }
}

function handleEvent9735(event: Event<9735>) {
  const activity = toZap(event);

  if (activity) {
    return federate(activity);
  }
}

/** A Nostr event is a repost if it's a quote post with no content. */
function isRepost(event: Event<1>): boolean {
  return Boolean((!event.content || /^#\[\d+\]$/.test(event.content)) && event.tags.find(isQuoteTag));
}

function isEventEligible(event: Event): boolean {
  // Ensure events from the bridge don't loop back.
  if (isProxyEvent(event)) {
    return false;
  }

  // Ensure events from ActivityPub users don't loop back.
  if (cipher.getApId(event.pubkey)) {
    return false;
  }

  // Handle events from users with ActivityPub followers.
  if (followsDB.hasFollowers(event.pubkey)) {
    return true;
  }

  // Fallback: handle events where an ActivityPub user was tagged.
  return event.tags.some((tag) => {
    return tag[0] === 'p' && cipher.getApId(tag[1]);
  });
}

export default handleEvent;
