import { LOCAL_DOMAIN } from '../config.ts';

import { getPublicKey } from './sign.ts';

/** Convert ActivityPub ID into Nostr pubkey. */
async function toPubkey(apId: string): Promise<string> {
  if (apId.startsWith(`${LOCAL_DOMAIN}/users/`)) {
    return new URL(apId).pathname.split('/')[2];
  } else {
    return await getPublicKey(apId);
  }
}

export { toPubkey };
