import { safeFetch } from '@/deps.ts';
import { isURL } from '@/utils/parse.ts';

import { followsDB } from '../db.ts';
import { signRequest } from '../deps.ts';
import { apIdToPubkey } from '../utils/parse.ts';

import { getPrivateKey } from './keys.ts';
import { isActor, isActorId, isLocalId, maybeAddContext } from './utils.ts';
import { fetchObject } from './client.ts';

import type { Activity } from './schema.ts';

const deduplicate = (items: string[]): string[] => Array.from(new Set(items));

async function getActorInbox(actorId: string): Promise<string | undefined> {
  const actor = await fetchObject(actorId);

  if (actor && isActor(actor)) {
    return actor.endpoints?.sharedInbox || actor.inbox;
  }
}

async function getInboxes(recipientIds: string[]): Promise<string[]> {
  const promises = recipientIds
    .filter((apId) => isActorId(apId) && !isLocalId(apId))
    .map(getActorInbox);

  const result = await Promise.all(promises);
  const inboxes = result.filter(isURL);

  return deduplicate(inboxes);
}

async function federate(object: Activity): Promise<(undefined | Response)[]> {
  console.log('Federating object', object);

  object = maybeAddContext(object);
  const body = JSON.stringify(object);
  const privateKey = await getPrivateKey(object.actor);

  const recipients = [...object.to, ...object.cc];

  if (recipients.some((r) => r === `${object.actor}/followers`)) {
    recipients.push(...followsDB.getFollowers(apIdToPubkey(object.actor)));
  }

  const inboxes = await getInboxes(recipients);

  const results = inboxes.map(async (inbox) => {
    try {
      const req = new Request(inbox, {
        method: 'POST',
        headers: {
          'content-type': 'application/activity+json',
          'user-agent': 'Mostr Bridge <https://mostr.pub/>',
        },
        body,
      });

      await signRequest(req, privateKey, `${object.actor}#main-key`);
      return safeFetch(req);
    } catch (e) {
      console.log(e);
    }
  });

  return Promise.all(results);
}

export { federate };
