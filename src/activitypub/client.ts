import { safeFetch } from '@/deps.ts';

import ExpiringCache from '../expiring-cache.ts';

import { Object, objectSchema } from './schema.ts';

const cache = new ExpiringCache(await caches.open('objects'));

/** Fetch object or get from cache. */
const fetchObject = async (url: string, timeout = 5000): Promise<Object | undefined> => {
  try {
    const cached = await cache.match(url);
    const schema = objectSchema.refine((obj) => obj.id === url); // Ensure matching ID.

    if (cached) {
      return schema.parse(await cached.json());
    } else {
      const response = await safeFetch(url, {
        headers: {
          accept: 'application/activity+json',
        },
        signal: AbortSignal.timeout(timeout),
      });

      if (response.ok) {
        cache.putExpiring(url, response.clone(), 3600);
      } else {
        cache.putExpiring(url, response.clone(), 30);
      }

      return schema.parse(await response.json());
    }
  } catch (_e) {
    return;
  }
};

export { fetchObject };
