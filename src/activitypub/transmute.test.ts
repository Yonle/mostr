import { assertEquals } from '@/deps.ts';

import { cipher } from '../db.ts';
import event0 from '../../fixtures/nostr/event-0.json' assert { type: 'json' };
import event0withLink from '../../fixtures/nostr/event-0-with-link.json' assert { type: 'json' };
import event1 from '../../fixtures/nostr/event-1.json' assert { type: 'json' };
import event1withLink from '../../fixtures/nostr/event-1-with-link.json' assert { type: 'json' };
import event1withLinkNoproto from '../../fixtures/nostr/event-1-with-link-noproto.json' assert { type: 'json' };
import event1withAttachment from '../../fixtures/nostr/event-1-with-attachment.json' assert { type: 'json' };
import event1withMentions from '../../fixtures/nostr/event-1-with-mentions.json' assert { type: 'json' };
import event1withLinebreaks from '../../fixtures/nostr/event-1-with-linebreaks.json' assert { type: 'json' };
import event1withHashtags from '../../fixtures/nostr/event-1-with-hashtags.json' assert { type: 'json' };
import event1withQuote from '../../fixtures/nostr/event-1-with-quote.json' assert { type: 'json' };
import event9735 from '../../fixtures/nostr/event-9735.json' assert { type: 'json' };
import nip27Mention from '../../fixtures/nostr/nip-27-mention.json' assert { type: 'json' };
import nip27Quote from '../../fixtures/nostr/nip-27-quote.json' assert { type: 'json' };

import { toActor, toNote, toZap } from './transmute.ts';

import type { Event } from '../nostr/event.ts';
import type { Actor, Note, Zap } from './schema.ts';

Deno.test('toActor', async () => {
  const actor = await toActor(event0 as Event<0>);

  const expected: Actor = {
    type: 'Person',
    id: 'http://localhost:8000/users/82341f882b6eabcd2ba7f1ef90aad961cf074af15b9ef44a09f9d2a8fbfbe6a2',
    name: 'jack',
    preferredUsername: '82341f882b6eabcd2ba7f1ef90aad961cf074af15b9ef44a09f9d2a8fbfbe6a2',
    inbox: 'http://localhost:8000/users/82341f882b6eabcd2ba7f1ef90aad961cf074af15b9ef44a09f9d2a8fbfbe6a2/inbox',
    followers: 'http://localhost:8000/users/82341f882b6eabcd2ba7f1ef90aad961cf074af15b9ef44a09f9d2a8fbfbe6a2/followers',
    following: 'http://localhost:8000/users/82341f882b6eabcd2ba7f1ef90aad961cf074af15b9ef44a09f9d2a8fbfbe6a2/following',
    outbox: 'http://localhost:8000/users/82341f882b6eabcd2ba7f1ef90aad961cf074af15b9ef44a09f9d2a8fbfbe6a2/outbox',
    icon: {
      type: 'Image',
      url: 'https://pbs.twimg.com/profile_images/1115644092329758721/AFjOr-K8_400x400.jpg',
    },
    image: {
      type: 'Image',
      url:
        'https://upload.wikimedia.org/wikipedia/commons/b/b4/The_Sun_by_the_Atmospheric_Imaging_Assembly_of_NASA%27s_Solar_Dynamics_Observatory_-_20100819.jpg',
    },
    summary: '<a class="mention hashtag" href="http://localhost:8000/tags/bitcoin" rel="tag"><span>#</span>bitcoin</a>',
    attachment: [],
    tag: [],
    publicKey: {
      id: 'http://localhost:8000/users/82341f882b6eabcd2ba7f1ef90aad961cf074af15b9ef44a09f9d2a8fbfbe6a2#main-key',
      owner: 'http://localhost:8000/users/82341f882b6eabcd2ba7f1ef90aad961cf074af15b9ef44a09f9d2a8fbfbe6a2',
      publicKeyPem:
        '-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCjTTdmJxPNeuZk4NTPwEPghBnn8mYgqKpiX4B+tNLxe2jLebhDe7B0s7BgL4npDAuqRi+91koXh69OV3ZZjBHmku/pwKEIxehsX4rqUCTA260EW25xcjO/4tmmc9YU0fmtD/+Cr2F7sYWsfJx/sBHMGyjLXTJDB24RpfygR7jHBQIDAQAB\n-----END PUBLIC KEY-----',
    },
    endpoints: { sharedInbox: 'http://localhost:8000/inbox' },
    proxyOf: [{
      protocol: 'https://github.com/nostr-protocol/nostr',
      proxied: 'npub1sg6plzptd64u62a878hep2kev88swjh3tw00gjsfl8f237lmu63q0uf63m',
      authoritative: true,
    }],
  };

  assertEquals(actor, expected);
});

Deno.test('toActor linkifies', async () => {
  const actor = await toActor(event0withLink as Event<0>);

  const expected: Actor = {
    type: 'Person',
    id: 'http://localhost:8000/users/103749fdadc55ae04f27c0087f2c3be5a0775bedac88648c0d84bff2d64efbec',
    name: 'alex',
    preferredUsername: '103749fdadc55ae04f27c0087f2c3be5a0775bedac88648c0d84bff2d64efbec',
    inbox: 'http://localhost:8000/users/103749fdadc55ae04f27c0087f2c3be5a0775bedac88648c0d84bff2d64efbec/inbox',
    followers: 'http://localhost:8000/users/103749fdadc55ae04f27c0087f2c3be5a0775bedac88648c0d84bff2d64efbec/followers',
    following: 'http://localhost:8000/users/103749fdadc55ae04f27c0087f2c3be5a0775bedac88648c0d84bff2d64efbec/following',
    outbox: 'http://localhost:8000/users/103749fdadc55ae04f27c0087f2c3be5a0775bedac88648c0d84bff2d64efbec/outbox',
    icon: undefined,
    image: undefined,
    summary: 'I love <a href="https://mastodon.social">https://mastodon.social</a>',
    attachment: [],
    tag: [],
    publicKey: {
      id: 'http://localhost:8000/users/103749fdadc55ae04f27c0087f2c3be5a0775bedac88648c0d84bff2d64efbec#main-key',
      owner: 'http://localhost:8000/users/103749fdadc55ae04f27c0087f2c3be5a0775bedac88648c0d84bff2d64efbec',
      publicKeyPem:
        '-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCKLO1eP41DAhdXt/U8NxtOL5fAFcPGFsQWjqMpZ1FrwL5+UBNH+6Gp3/nntLP3nlE+JvFNf9MMxXztxWXbXAwsHIRnfFV/pb7TrCT+HZXizLY2R1W0mK71N7Tu5AJC0RAPWT90qzKbKLZ5ab8IBAgEGimhOdeaZiVvfXKeBaipJQIDAQAB\n-----END PUBLIC KEY-----',
    },
    endpoints: { sharedInbox: 'http://localhost:8000/inbox' },
    proxyOf: [{
      protocol: 'https://github.com/nostr-protocol/nostr',
      proxied: 'npub1zqm5nlddc4dwqne8cqy87tpmuks8wkld4jyxfrqdsjll94jwl0kqtmhka0',
      authoritative: true,
    }],
  };

  assertEquals(actor, expected);
});

Deno.test('toNote', async () => {
  const note = await toNote(event1 as Event<1>);

  const expected: Note = {
    type: 'Note',
    id: 'http://localhost:8000/objects/e3460f9dbe3c41c7e2e26290d08b39ee997a1f1a42702f5db6385369ff6b7093',
    attributedTo: 'http://localhost:8000/users/82341f882b6eabcd2ba7f1ef90aad961cf074af15b9ef44a09f9d2a8fbfbe6a2',
    content: 'is there an alien invasion happening right now?',
    to: [
      'https://www.w3.org/ns/activitystreams#Public',
    ],
    cc: [
      'http://localhost:8000/users/82341f882b6eabcd2ba7f1ef90aad961cf074af15b9ef44a09f9d2a8fbfbe6a2/followers',
    ],
    tag: [],
    attachment: [],
    published: '2023-02-12T02:47:38.000Z',
    inReplyTo: undefined,
    quoteUrl: undefined,
    sensitive: false,
    summary: undefined,
    proxyOf: [{
      protocol: 'https://github.com/nostr-protocol/nostr',
      proxied: 'note1udrql8d783qu0chzv2gdpzeea6vh58c6gfcz7hdk8pfknlmtwzfs5805t6',
      authoritative: true,
    }],
  };

  assertEquals(note, expected);
});

Deno.test('toNote linkifies', async () => {
  const note = await toNote(event1withLink as Event<1>);

  const expected: Note = {
    type: 'Note',
    id: 'http://localhost:8000/objects/3708a7e76be406e0903047430c0fc09b0991593b5bedaf3094980c5bdd4deaee',
    attributedTo: 'http://localhost:8000/users/103749fdadc55ae04f27c0087f2c3be5a0775bedac88648c0d84bff2d64efbec',
    content:
      'hello <a href="https://mastodon.social">https://mastodon.social</a> &lt;script src="xss.js"&gt;&lt;/script&gt;',
    to: [
      'https://www.w3.org/ns/activitystreams#Public',
    ],
    cc: [
      'http://localhost:8000/users/103749fdadc55ae04f27c0087f2c3be5a0775bedac88648c0d84bff2d64efbec/followers',
    ],
    tag: [],
    attachment: [],
    published: '2023-02-28T21:09:35.000Z',
    inReplyTo: undefined,
    quoteUrl: undefined,
    sensitive: false,
    summary: undefined,
    proxyOf: [{
      protocol: 'https://github.com/nostr-protocol/nostr',
      proxied: 'note1xuy20emtusrwpypsgapscr7qnvyezkfmt0k67vy5nqx9hh2dathqu5l7md',
      authoritative: true,
    }],
  };

  assertEquals(note, expected);
});

Deno.test('toNote linkifies URLs without a protocol correctly', async () => {
  const note = await toNote(event1withLinkNoproto as Event<1>);

  const expected: Note = {
    type: 'Note',
    id: 'http://localhost:8000/objects/2e49c7e1e75b3441a13614acc6bba3e23ba4497f72232cf56a277c2b905226a3',
    attributedTo: 'http://localhost:8000/users/9f150df609a0262f6663ddd7c30c3d060818161bdce5a9b31fe2618c9c0a050a',
    content: 'checkout <a href="http://x.com">x.com</a> (not really)',
    to: ['https://www.w3.org/ns/activitystreams#Public'],
    cc: [
      'http://localhost:8000/users/9f150df609a0262f6663ddd7c30c3d060818161bdce5a9b31fe2618c9c0a050a/followers',
    ],
    tag: [],
    attachment: [],
    sensitive: false,
    summary: undefined,
    published: '2023-07-24T22:28:01.000Z',
    inReplyTo: undefined,
    quoteUrl: undefined,
    proxyOf: [
      {
        protocol: 'https://github.com/nostr-protocol/nostr',
        proxied: 'note19eyu0c08tv6yrgfkzjkvdwaruga6gjtlwg3jeat2ya7zhyzjy63s3ujjmv',
        authoritative: true,
      },
    ],
  };

  assertEquals(note, expected);
});

Deno.test({
  name: 'toNote parses mentions',
  fn: async () => {
    cipher.add({
      apId: 'https://gleasonator.com/users/alex',
      nostrId: 'a06f17ffb19bf45857e5974ac8bb1bea578189786eea458e443bdf8c3d533bd3',
    });

    cipher.add({
      apId: 'https://poa.st/users/graf',
      nostrId: '9b9b4d879acf5214aa7ce7b3e8e0328431ce3c0d898fa1817e0565160ef03b82',
    });

    cipher.add({
      apId: 'https://gleasonator.com/objects/f969c233-68dc-437a-8a56-3c7a5d5e2e07',
      nostrId: '3d4ece2428b7dfd80061ef20232baa6c807d817ba6b1e034a7ce233ff3f4b049',
    });

    const note = await toNote(event1withMentions as Event<1>);

    const expected: Note = {
      type: 'Note',
      id: 'http://localhost:8000/objects/9329f8e49138586937fa5b8c5c3887a77c0468ba83d8d8a1c97266f7ab0a04ce',
      attributedTo: 'http://localhost:8000/users/9f150df609a0262f6663ddd7c30c3d060818161bdce5a9b31fe2618c9c0a050a',
      content:
        'hello <span class="h-card"><a class="u-url mention" href="https://gleasonator.com/users/alex" rel="ugc">@<span>alex</span></a></span> @npub15ph30la3n069s4l9ja9v3wcmaftcrztcdm4ytrjy800cc02n80fsqvkw0q  and <span class="h-card"><a class="u-url mention" href="https://poa.st/users/graf" rel="ugc">@<span>graf</span></a></span>',
      to: [
        'https://www.w3.org/ns/activitystreams#Public',
        'https://gleasonator.com/users/alex',
        'https://poa.st/users/graf',
      ],
      cc: [
        'http://localhost:8000/users/9f150df609a0262f6663ddd7c30c3d060818161bdce5a9b31fe2618c9c0a050a/followers',
      ],
      tag: [
        {
          type: 'Mention',
          href: 'https://gleasonator.com/users/alex',
          name: '@alex@gleasonator.com',
        },
        { type: 'Mention', href: 'https://poa.st/users/graf', name: '@graf@poa.st' },
      ],
      attachment: [],
      published: '2023-03-03T21:20:14.000Z',
      inReplyTo: 'https://gleasonator.com/objects/f969c233-68dc-437a-8a56-3c7a5d5e2e07',
      quoteUrl: undefined,
      sensitive: false,
      summary: undefined,
      proxyOf: [{
        protocol: 'https://github.com/nostr-protocol/nostr',
        proxied: 'note1jv5l3ey38pvxjdl6twx9cwy85a7qg696s0vd3gwfwfn002c2qn8qg3df9q',
        authoritative: true,
      }],
    };

    assertEquals(note, expected);
  },
  sanitizeResources: false,
  sanitizeOps: false,
});

Deno.test({
  name: 'toNote parses NIP-27 mentions',
  fn: async () => {
    const note = await toNote(nip27Mention as Event<1>);

    const expectedContent =
      'hello <span class="h-card"><a class="u-url mention" href="http://localhost:8000/users/2c7cc62a697ea3a7826521f3fd34f0cb273693cbe5e9310f35449f43622a5cdc" rel="ugc">@<span>2c7cc62a</span></a></span>';

    assertEquals(note.content, expectedContent);

    const expected: Note = {
      type: 'Note',
      id: 'http://localhost:8000/objects/f39e9b451a73d62abc5016cffdd294b1a904e2f34536a208874fe5e22bbd47cf',
      attributedTo: 'http://localhost:8000/users/79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798',
      content: expectedContent,
      to: [
        'https://www.w3.org/ns/activitystreams#Public',
        'http://localhost:8000/users/2c7cc62a697ea3a7826521f3fd34f0cb273693cbe5e9310f35449f43622a5cdc',
      ],
      cc: [
        'http://localhost:8000/users/79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798/followers',
      ],
      tag: [
        {
          type: 'Mention',
          href: 'http://localhost:8000/users/2c7cc62a697ea3a7826521f3fd34f0cb273693cbe5e9310f35449f43622a5cdc',
          name: '@2c7cc62a697ea3a7826521f3fd34f0cb273693cbe5e9310f35449f43622a5cdc@localhost:8000',
        },
      ],
      attachment: [],
      published: '2023-03-26T00:32:54.000Z',
      inReplyTo: undefined,
      quoteUrl: undefined,
      sensitive: false,
      summary: undefined,
      proxyOf: [{
        protocol: 'https://github.com/nostr-protocol/nostr',
        proxied: 'note17w0fk3g6w0tz40zszm8lm555kx5sfchng5m2yzy8flj7y2aagl8s8td87j',
        authoritative: true,
      }],
    };

    assertEquals(note, expected);
  },
  sanitizeResources: false,
  sanitizeOps: false,
});

Deno.test({
  name: 'toNote parses NIP-27 quotes',
  fn: async () => {
    cipher.add({
      apId: 'https://gleasonator.com/users/alex',
      nostrId: '79c2cae114ea28a981e7559b4fe7854a473521a8d22a66bbab9fa248eb820ff6',
    });

    cipher.add({
      apId: 'https://gleasonator.com/objects/eb208601-1b6c-4ba7-bd82-2296fa93b5b0',
      nostrId: '46d731680add2990efe1cc619dc9b8014feeb23261ab9dee50e9d11814de5a2b',
    });

    const note = await toNote(nip27Quote as Event<1>);

    const expected: Note = {
      type: 'Note',
      id: 'http://localhost:8000/objects/c9977e2fae3ed8016e9496612a90cee99f61968a2cab761681de6c1e2f2bb862',
      attributedTo: 'http://localhost:8000/users/32e1827635450ebb3c5a7d12c1f8e7b2b514439ac10a67eef3d9fd9c5c68e245',
      content:
        'Thanks to these strfry policy scripts from <span class="h-card"><a class="u-url mention" href="https://gleasonator.com/users/alex" rel="ugc">@<span>alex</span></a></span>​ , the damus relay universe feed should be back again! I added rate limiting, duplicate message and hellthread blocking. Less spam! Yay!',
      to: [
        'https://www.w3.org/ns/activitystreams#Public',
        'https://gleasonator.com/users/alex',
      ],
      cc: [
        'http://localhost:8000/users/32e1827635450ebb3c5a7d12c1f8e7b2b514439ac10a67eef3d9fd9c5c68e245/followers',
      ],
      tag: [
        {
          type: 'Mention',
          href: 'https://gleasonator.com/users/alex',
          name: '@alex@gleasonator.com',
        },
      ],
      attachment: [],
      published: '2023-04-18T15:12:17.000Z',
      inReplyTo: undefined,
      quoteUrl: 'https://gleasonator.com/objects/eb208601-1b6c-4ba7-bd82-2296fa93b5b0',
      sensitive: false,
      summary: undefined,
      proxyOf: [{
        protocol: 'https://github.com/nostr-protocol/nostr',
        proxied: 'note1exthutaw8mvqzm55jesj4yxwax0kr9529j4hv95pmekputethp3qjql8n7',
        authoritative: true,
      }],
    };

    assertEquals(note, expected);
  },
  sanitizeResources: false,
  sanitizeOps: false,
});

Deno.test({
  name: 'toNote renders an attachment',
  fn: async () => {
    const note = await toNote(event1withAttachment as Event<1>);

    const expected: Note = {
      type: 'Note',
      id: 'http://localhost:8000/objects/a48ee5ea983d1af41af70bd7cf4231096da0d1db65bc4dbc09b6526119c528b1',
      attributedTo: 'http://localhost:8000/users/80f48f5e58c5d0d9774856ebf7f619da347b1546fc0115dbcb6511de9d4a9e3c',
      content:
        'Crypto lol<br /><br /><a href="https://nostr.build/i/nostr.build_98cfd6412151849ab1d176f7069da25a76793f8a429746d388054776cf9f4a96.png">https://nostr.build/i/nostr.build_98cfd6412151849ab1d176f7069da25a76793f8a429746d388054776cf9f4a96.png</a>',
      to: ['https://www.w3.org/ns/activitystreams#Public'],
      cc: [
        'http://localhost:8000/users/80f48f5e58c5d0d9774856ebf7f619da347b1546fc0115dbcb6511de9d4a9e3c/followers',
      ],
      tag: [],
      attachment: [
        {
          type: 'Document',
          url: 'https://nostr.build/i/nostr.build_98cfd6412151849ab1d176f7069da25a76793f8a429746d388054776cf9f4a96.png',
          mediaType: 'image/png',
        },
      ],
      published: '2023-03-08T15:28:18.000Z',
      inReplyTo: undefined,
      quoteUrl: undefined,
      sensitive: false,
      summary: undefined,
      proxyOf: [{
        protocol: 'https://github.com/nostr-protocol/nostr',
        proxied: 'note15j8wt65c85d0gxhhp0tu7s33p9k6p5wmvk7ym0qfkefxzxw99zcs82lexe',
        authoritative: true,
      }],
    };

    assertEquals(note, expected);
  },
  sanitizeResources: false,
  sanitizeOps: false,
});

Deno.test({
  name: 'toNote handles sensitive content',
  fn: async () => {
    const note = await toNote({
      kind: 1,
      content: '',
      created_at: 0,
      tags: [['content-warning', 'boooo']],
      pubkey: '331e88071f87adf58df79c88e8168c70595c9c21014de3e32784b83c2d93f85a',
      id: 'f379a5362389ec997d81d27c4956fac85c21faae3def1c43d37abbe9833ec7c4',
      sig:
        'b98d60bbcb0737c8323afc42ce27800b532319b418c160974a0969fc35f65db94d76ca408520633538f42941e7d584798dd83b5b9e84c8cd64cd3d9d879726eb',
    });

    const expected: Note = {
      type: 'Note',
      id: 'http://localhost:8000/objects/f379a5362389ec997d81d27c4956fac85c21faae3def1c43d37abbe9833ec7c4',
      attributedTo: 'http://localhost:8000/users/331e88071f87adf58df79c88e8168c70595c9c21014de3e32784b83c2d93f85a',
      content: '',
      to: ['https://www.w3.org/ns/activitystreams#Public'],
      cc: [
        'http://localhost:8000/users/331e88071f87adf58df79c88e8168c70595c9c21014de3e32784b83c2d93f85a/followers',
      ],
      tag: [],
      attachment: [],
      sensitive: true,
      summary: 'boooo',
      published: '1970-01-01T00:00:00.000Z',
      inReplyTo: undefined,
      quoteUrl: undefined,
      proxyOf: [{
        protocol: 'https://github.com/nostr-protocol/nostr',
        proxied: 'note17du62d3r38kfjlvp6f7yj4h6epwzr74w8hh3cs7n02a7nqe7clzqxg3mge',
        authoritative: true,
      }],
    };

    assertEquals(note, expected);
  },
  sanitizeResources: false,
  sanitizeOps: false,
});

Deno.test({
  name: 'toNote converts newlines to <br /> tags',
  fn: async () => {
    const note = await toNote(event1withLinebreaks as Event<1>);

    const expected: Note = {
      type: 'Note',
      id: 'http://localhost:8000/objects/97a2eadd56ae222d1caa32a84c01c9d989f414fc6a0219088d21d93dff41338f',
      attributedTo: 'http://localhost:8000/users/c708943ea349519dcf56b2a5c138fd9ed064ad65ddecae6394eabd87a62f1770',
      content:
        'the mental model i have adopted for likes, reposts and zaps is as follows:<br /><br />like = good<br />repost = very good<br />zap = excellent<br /><br />on a 5-point scale:<br /><br />like = 3<br />repost = 4<br />zap = 5',
      to: ['https://www.w3.org/ns/activitystreams#Public'],
      cc: [
        'http://localhost:8000/users/c708943ea349519dcf56b2a5c138fd9ed064ad65ddecae6394eabd87a62f1770/followers',
      ],
      tag: [],
      attachment: [],
      sensitive: false,
      summary: undefined,
      published: '2023-03-18T18:35:07.000Z',
      inReplyTo: undefined,
      quoteUrl: undefined,
      proxyOf: [{
        protocol: 'https://github.com/nostr-protocol/nostr',
        proxied: 'note1j73w4h2k4c3z6892x25ycqwfmxylg98udgppjzydy8vnml6pxw8s4xy3kq',
        authoritative: true,
      }],
    };

    assertEquals(note, expected);
  },
  sanitizeResources: false,
  sanitizeOps: false,
});

Deno.test({
  name: 'toNote handles hashtags',
  fn: async () => {
    const note = await toNote(event1withHashtags as Event<1>);

    const expected: Note = {
      type: 'Note',
      id: 'http://localhost:8000/objects/2e8c0baeae2642ad200e44bc94780e076c41aeaeb39e0deb12d8d1b24dbe614f',
      attributedTo: 'http://localhost:8000/users/c48e29f04b482cc01ca1f9ef8c86ef8318c059e0e9353235162f080f26e14c11',
      content:
        '<a class="mention hashtag" href="http://localhost:8000/tags/Nostrica" rel="tag"><span>#</span>Nostrica</a> <a href="https://nostr.build/i/nostr.build_2d9e8e5a6f3018f3f6c77fbb733403f64e080878958315a16feef7d270633640.jpg">https://nostr.build/i/nostr.build_2d9e8e5a6f3018f3f6c77fbb733403f64e080878958315a16feef7d270633640.jpg</a>',
      to: ['https://www.w3.org/ns/activitystreams#Public'],
      cc: [
        'http://localhost:8000/users/c48e29f04b482cc01ca1f9ef8c86ef8318c059e0e9353235162f080f26e14c11/followers',
      ],
      tag: [
        { type: 'Hashtag', href: 'http://localhost:8000/tags/nostrica', name: '#nostrica' },
      ],
      attachment: [
        {
          type: 'Document',
          url: 'https://nostr.build/i/nostr.build_2d9e8e5a6f3018f3f6c77fbb733403f64e080878958315a16feef7d270633640.jpg',
          mediaType: 'image/jpeg',
        },
      ],
      sensitive: false,
      summary: undefined,
      published: '2023-03-19T14:49:10.000Z',
      inReplyTo: undefined,
      quoteUrl: undefined,
      proxyOf: [{
        protocol: 'https://github.com/nostr-protocol/nostr',
        proxied: 'note196xqht4wyep26gqwgj7fg7qwqakyrt4wkw0qm6cjmrgmynd7v98s8sgf0p',
        authoritative: true,
      }],
    };

    assertEquals(note, expected);
  },
  sanitizeResources: false,
  sanitizeOps: false,
});

Deno.test({
  name: 'toNote strips #[0] tokens from quotes',
  fn: async () => {
    const note = await toNote(event1withQuote as Event<1>);

    const expected: Note = {
      type: 'Note',
      id: 'http://localhost:8000/objects/5258b0ccfb5d1d16564fe2df7c495ee754b5fd43d2f472ab3c05cc5ef17e69b6',
      attributedTo: 'http://localhost:8000/users/9c163c7351f8832b08b56cbb2e095960d1c5060dd6b0e461e813f0f07459119e',
      content:
        '<a class="mention hashtag" href="http://localhost:8000/tags/VirtualLunchHour" rel="tag"><span>#</span>VirtualLunchHour</a>',
      to: ['https://www.w3.org/ns/activitystreams#Public'],
      cc: [
        'http://localhost:8000/users/9c163c7351f8832b08b56cbb2e095960d1c5060dd6b0e461e813f0f07459119e/followers',
      ],
      tag: [
        {
          type: 'Hashtag',
          href: 'http://localhost:8000/tags/virtuallunchhour',
          name: '#virtuallunchhour',
        },
      ],
      attachment: [],
      sensitive: false,
      summary: undefined,
      published: '2023-03-19T17:54:20.000Z',
      inReplyTo: 'http://localhost:8000/objects/8a31b8aababd6111d0486cbb43a29929372a88ecc2540a06111416af7fb789ec',
      quoteUrl: 'http://localhost:8000/objects/8a31b8aababd6111d0486cbb43a29929372a88ecc2540a06111416af7fb789ec',
      proxyOf: [{
        protocol: 'https://github.com/nostr-protocol/nostr',
        proxied: 'note12fvtpn8mt5w3v4j0ut0hcj27ua2ttl2r6t6892euqhx9aut7dxmqgz29f6',
        authoritative: true,
      }],
    };

    assertEquals(note, expected);
  },
  sanitizeResources: false,
  sanitizeOps: false,
});

Deno.test({
  name: 'toZap',
  fn: () => {
    const zap = toZap(event9735 as Event<9735>);

    const expected: Zap = {
      type: 'Zap',
      id: 'http://localhost:8000/objects/67b48a14fb66c60c8f9070bdeb37afdfcc3d08ad01989460448e4081eddda446',
      object: 'http://localhost:8000/objects/3624762a1274dd9636e0c552b53086d70bc88c165bc4dc0f9e836a1eaf86c3b8',
      actor: 'http://localhost:8000/users/32e1827635450ebb3c5a7d12c1f8e7b2b514439ac10a67eef3d9fd9c5c68e245',
      to: [
        'https://www.w3.org/ns/activitystreams#Public',
        'http://localhost:8000/users/32e1827635450ebb3c5a7d12c1f8e7b2b514439ac10a67eef3d9fd9c5c68e245',
      ],
      cc: [
        'http://localhost:8000/users/32e1827635450ebb3c5a7d12c1f8e7b2b514439ac10a67eef3d9fd9c5c68e245/followers',
      ],
      proxyOf: [{
        protocol: 'https://github.com/nostr-protocol/nostr',
        proxied: 'note1v76g598mvmrqeruswz77kda0mlxr6z9dqxvfgczy3eqgrmwa53rqhhsxmc',
        authoritative: true,
      }],
    };

    assertEquals(zap, expected);
  },
  sanitizeResources: false,
  sanitizeOps: false,
});
