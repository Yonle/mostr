import { cipher } from '../../db.ts';
import { fetchFollows } from '../../nostr/client.ts';
import { isPubkeyTag } from '../../nostr/tags.ts';
import { url } from '../../utils/parse.ts';

import { activityJson } from './utils.ts';

import type { Context } from '../../deps.ts';

async function followingController(c: Context<'pubkey'>) {
  const pubkey = c.req.param('pubkey');

  const event = await fetchFollows(pubkey);
  const tags = event?.tags || [];

  const items = tags
    .filter(isPubkeyTag)
    .map((tag) => cipher.getApId(tag[1]) || url(`/users/${tag[1]}`))
    .filter(Boolean);

  return activityJson(c, {
    id: url(`/${pubkey}/following`),
    type: 'OrderedCollection',
    totalItems: items.length,
    first: {
      id: url(`/${pubkey}/following?page=1`),
      type: 'OrderedCollectionPage',
      orderedItems: items,
      partOf: url(`/${pubkey}/following`),
      totalItems: items.length,
    },
  });
}

export { followingController };
