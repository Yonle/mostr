import { cipher } from '../../db.ts';
import { fetchUser } from '../../nostr/client.ts';
import { isProxyEvent } from '../../nostr/tags.ts';
import { toActor } from '../transmute.ts';

import { activityJson } from './utils.ts';

import type { Context } from '../../deps.ts';

async function actorController(c: Context<'pubkey'>) {
  const pubkey = c.req.param('pubkey');

  if (cipher.getApId(pubkey)) {
    return c.json({ error: 'Not found' }, 404);
  }

  const event = await fetchUser(pubkey);

  if (event && !isProxyEvent(event)) {
    const actor = await toActor(event);
    return activityJson(c, actor);
  } else {
    return c.json({ error: 'Not found' }, 404);
  }
}

export { actorController };
