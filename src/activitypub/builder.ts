import { generateId } from '../utils/id.ts';
import { AP_PUBLIC_URI } from './constants.ts';

import type { Activity, Actor, Follow, Note } from './schema.ts';

const buildAccept = (follow: Follow): Activity => {
  return {
    type: 'Accept',
    id: generateId('activities'),
    actor: follow.object,
    object: follow.id,
    to: [follow.actor],
    cc: [],
  };
};

function buildFollow(actorId: string, objectId: string): Follow {
  return {
    type: 'Follow',
    id: generateId('activities'),
    actor: actorId,
    object: objectId,
    to: [objectId],
    cc: [],
  };
}

const wrapCreate = (object: Note): Activity => {
  // HACK: actually parse the URL
  const id = object.id.replace('/objects/', '/activities/');

  return {
    type: 'Create',
    id,
    actor: object.attributedTo,
    object,
    to: object.to,
    cc: object.cc,
    published: object.published,
  };
};

const wrapUpdate = (object: Actor): Activity => {
  return {
    type: 'Update',
    id: generateId('activities'),
    actor: object.id,
    object,
    to: [AP_PUBLIC_URI, `${object.id}/followers`],
    cc: [],
  };
};

export { buildAccept, buildFollow, wrapCreate, wrapUpdate };
