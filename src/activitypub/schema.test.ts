import { assertThrows } from '@/deps.ts';

import { Activity, activitySchema } from './schema.ts';

Deno.test('activitySchema containment', () => {
  const valid: Activity = {
    type: 'Create',
    id: 'https://gleasonator.com/activities/123',
    actor: 'https://gleasonator.com/users/alex',
    to: [],
    cc: [],
    object: {
      type: 'Note',
      id: 'https://gleasonator.com/objects/456',
      attributedTo: 'https://gleasonator.com/users/alex',
      to: [],
      cc: [],
      tag: [],
      content: 'hello world',
      published: new Date().toISOString(),
    },
    published: new Date().toISOString(),
  };

  activitySchema.parse(valid);

  const invalid: Activity = {
    type: 'Create',
    id: 'https://gleasonator.com/activities/123',
    actor: 'https://gleasonator.com/users/alex',
    to: [],
    cc: [],
    object: {
      type: 'Note',
      id: 'https://mastodon.social/objects/456',
      attributedTo: 'https://mastodon.social/users/alex',
      to: [],
      cc: [],
      tag: [],
      content: 'hello world',
      published: new Date().toISOString(),
    },
    published: new Date().toISOString(),
  };

  assertThrows(() => {
    activitySchema.parse(invalid);
  });
});
